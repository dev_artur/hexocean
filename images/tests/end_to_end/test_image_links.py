from uuid import uuid4

import pytest
from django.test import Client
from django.utils import timezone
from rest_framework import status

from accounts.models import ThumbnailSize
from images.models import ImageLink
from images.models import UserImage
from images.tests.utils import get_test_image_file
from project.factories import UserFactory


@pytest.mark.django_db
class TestImageLinkView:

    def setup_method(self):
        self.client = Client()

    def test_user_can_acces_image_via_custom_link(self):
        user = UserFactory.create()
        image = get_test_image_file()
        user_image = UserImage.objects.create(user=user, image=image)
        thumbnail_size = ThumbnailSize.objects.create(
            height=200,
            name='Small',
            code=1
        )
        link_uuid = str(uuid4())
        ImageLink.objects.create(
            user_image=user_image,
            thumbnail_size=thumbnail_size,
            uuid=link_uuid
        )

        response = self.client.get(f'/media/{link_uuid}/')

        assert response.status_code == status.HTTP_200_OK

    def test_user_cannot_acces_expired_image_link(self):
        user = UserFactory.create()
        image = get_test_image_file()
        user_image = UserImage.objects.create(user=user, image=image)
        thumbnail_size = ThumbnailSize.objects.create(
            height=200,
            name='Small',
            code=1
        )
        link_uuid = str(uuid4())
        ImageLink.objects.create(
            user_image=user_image,
            thumbnail_size=thumbnail_size,
            uuid=link_uuid,
            expires=timezone.now()
        )

        response = self.client.get(f'/media/{link_uuid}/')

        assert response.status_code == status.HTTP_404_NOT_FOUND