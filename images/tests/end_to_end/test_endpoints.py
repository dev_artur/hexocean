from collections import OrderedDict

import pytest
from django.test import Client
from rest_framework import status

from accounts.models import Plan
from images.models import ImageLink
from images.models import UserImage
from images.tests.utils import get_test_image_file
from project.factories import UserFactory


@pytest.mark.django_db
class TestImagesView:

    def setup_method(self):
        self.client = Client()
        self.user = UserFactory.create()

    def test_posting_returns_401_if_user_not_authenticated(self):
        endpoint_url = '/api/images/upload_image/'
        image_file = get_test_image_file()
        response = self.client.post(
            endpoint_url,
            data={'image': image_file},
            format='multipart',
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_posting_an_image_saves_user_image(self):
        endpoint_url = '/api/images/upload_image/'
        image_file = get_test_image_file()
        self.client.force_login(self.user)
        response = self.client.post(
            endpoint_url,
            data={'image': image_file},
            format='multipart',
        )
        assert response.status_code == status.HTTP_201_CREATED

        user_images = UserImage.objects.filter(user=self.user)
        assert user_images.count() == 1

        user_image = user_images.last()
        expected_data = {
            'id': user_image.id,
            'generate_link_url': f'http://testserver/api/images/'
                                 f'{user_image.id}/generate_link/',
            'name': user_image.image.name
        }

        assert response.data == expected_data

    def test_posting_without_image_returns_bad_request(self):
        endpoint_url = '/api/images/upload_image/'
        self.client.force_login(self.user)
        response = self.client.post(
            endpoint_url,
            data={},
            format='multipart',
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_posting_an_unsupported_image_type_returns_bad_request(self):
        endpoint_url = '/api/images/upload_image/'
        image_file = get_test_image_file('bmp')
        self.client.force_login(self.user)
        response = self.client.post(
            endpoint_url,
            data={'image': image_file},
            format='multipart',
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_listing_images_returns_401_if_user_not_authenticated(self):
        endpoint_url = '/api/images/'
        response = self.client.get(endpoint_url)

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_user_can_get_the_list_of_all_their_images(self):
        other_user = UserFactory.create()
        user_image_1 = UserImage.objects.create(
            user=self.user, image=get_test_image_file(name='test1')
        )
        user_image_2 = UserImage.objects.create(
            user=self.user, image=get_test_image_file(name='test2')
        )
        UserImage.objects.create(
            user=other_user, image=get_test_image_file(name='test3')
        )

        endpoint_url = '/api/images/'
        self.client.force_login(self.user)
        response = self.client.get(endpoint_url)

        expected_data = [
            OrderedDict([
                ('id', user_image_1.id),
                ('generate_link_url', f'http://testserver/api/images/'
                                      f'{user_image_1.id}/generate_link/'),
                ('name', user_image_1.image.name)
            ]),
            OrderedDict([
                ('id', user_image_2.id),
                ('generate_link_url', f'http://testserver/api/images/'
                                      f'{user_image_2.id}/generate_link/'),
                ('name', user_image_2.image.name)
            ])
        ]

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 2
        assert response.data == expected_data


@pytest.mark.django_db
class TestGenerateLinkView:

    def setup_method(self):
        self.client = Client()
        self.user = UserFactory.create()
        self.plan = Plan.objects.create(name='Basic', original_images=True)
        self.user.account.plan = self.plan
        self.user.account.save()

    def test_post_returns_401_if_user_not_authenticated(self):
        image_file = get_test_image_file()
        user_image = UserImage.objects.create(
            user=self.user,
            image=image_file
        )

        response = self.client.post(
            f'/api/images/{user_image.id}/generate_link/',
            data={
                'size': 0
            },
            content_type='application/json'
        )

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_user_can_generate_link_for_uploaded_image(self):
        image_file = get_test_image_file()
        user_image = UserImage.objects.create(
            user=self.user,
            image=image_file
        )
        self.client.force_login(self.user)

        response = self.client.post(
            f'/api/images/{user_image.id}/generate_link/',
            data={
                'size': 0
            },
            content_type='application/json'
        )

        image_link = ImageLink.objects.last()

        assert response.status_code == status.HTTP_200_OK
        assert response.data['link'].endswith(f'/media/{image_link.uuid}/')