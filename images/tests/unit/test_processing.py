import pytest

from images.processing import get_resized_image
from images.tests.utils import get_test_image_file

resize_image_test_data = (
    ((1200, 400), 400, 400),
    ((200, 200), 400, 200),
    ((1111, 777), 400, 400),
)


class TestGetResizedImage:

    @pytest.mark.parametrize('size, height, expected', resize_image_test_data)
    def test_resizes_file_to_expected_height(self, size, height, expected):
        image = get_test_image_file(size=size)
        resized_image = get_resized_image(image, height=height)

        assert resized_image.size[1] == expected

    def test_does_not_resize_if_original_height_smaller_than_expected(self):
        image = get_test_image_file(size=(200, 200))
        resized_image = get_resized_image(image, height=400)

        assert resized_image.size[1] == 200