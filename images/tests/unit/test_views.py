from uuid import uuid4

import pytest
from django.test import Client
from django.utils import timezone
from rest_framework import status

from accounts.models import Plan
from accounts.models import ThumbnailSize
from images.models import ImageLink
from images.models import UserImage
from images.tests.utils import get_test_image_file
from project.factories import UserFactory


@pytest.mark.django_db
class TestImagesView:

    def setup_method(self):
        self.client = Client()
        self.upload_url = '/api/images/upload_image/'
        self.image = get_test_image_file()
        self.user = UserFactory.create()

    def test_view_returns_status_code_created(self):
        self.client.force_login(self.user)
        response = self.client.post(
            self.upload_url,
            {'image': self.image},
            format='multipart',
        )

        assert response.status_code == status.HTTP_201_CREATED

    def test_view_saves_the_uploaded_file_for_the_user(self):
        self.client.force_login(self.user)
        self.client.post(
            self.upload_url,
            {'image': self.image},
            format='multipart',
        )

        user_images = UserImage.objects.filter(user=self.user)
        assert user_images.count() == 1

    def test_view_returns_serialized_data_for_uploaded_image(self):
        self.client.force_login(self.user)
        response = self.client.post(
            self.upload_url,
            {'image': self.image},
            format='multipart',
        )

        user_image = UserImage.objects.last()
        expected_data = {
            'id': user_image.id,
            'generate_link_url': f'http://testserver/api/images/'
                                 f'{user_image.id}/generate_link/',
            'name': user_image.image.name
        }

        assert response.data == expected_data


@pytest.mark.django_db
class TestImagesListView:

    def setup_method(self):
        self.client = Client()
        self.list_url = '/api/images/'
        self.user = UserFactory.create()

    def test_view_returns_status_ok(self):
        self.client.force_login(self.user)
        response = self.client.get(self.list_url)

        assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
class TestImageLinkView:

    def setup_method(self):
        self.client = Client()
        self.link_url = '/media/'
        self.user = UserFactory.create()
        self.image = get_test_image_file()
        self.user_image = UserImage.objects.create(
            user=self.user, image=self.image
        )
        self.thumbnail_size = ThumbnailSize.objects.create(
            height=200,
            name='Small',
            code=1
        )
        self.link_uuid = str(uuid4())

    def test_view_returns_status_ok(self):
        ImageLink.objects.create(
            user_image=self.user_image,
            thumbnail_size=self.thumbnail_size,
            uuid=self.link_uuid
        )

        response = self.client.get(f'{self.link_url}{self.link_uuid}/')

        assert response.status_code == status.HTTP_200_OK

    def test_view_returns_status_not_found_when_link_expired(self):
        ImageLink.objects.create(
            user_image=self.user_image,
            thumbnail_size=self.thumbnail_size,
            uuid=self.link_uuid,
            expires=timezone.now()
        )

        response = self.client.get(f'{self.link_url}{self.link_uuid}/')

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.skip
    def test_view_returns_correct_file(self):
        ImageLink.objects.create(
            user_image=self.user_image,
            thumbnail_size=self.thumbnail_size,
            uuid=self.link_uuid
        )

        response = self.client.get(f'{self.link_url}{self.link_uuid}/')

        assert self.user_image.image.file.name.endswith(response.filename)

    def test_view_returns_status_not_found_when_uuid_not_existing(self):
        ImageLink.objects.create(
            user_image=self.user_image,
            thumbnail_size=self.thumbnail_size,
            uuid=self.link_uuid
        )
        other_uuid = str(uuid4())

        response = self.client.get(f'{self.link_url}{other_uuid}/')

        assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
class TestGenerateLinkView:

    def setup_method(self):
        self.client = Client()
        self.user = UserFactory.create()
        self.endpoint_url = '/api/images/'
        self.image_file = get_test_image_file()
        self.user_image = UserImage.objects.create(
            user=self.user,
            image=self.image_file
        )
        self.plan = Plan.objects.create(name='Basic', original_images=True)
        self.user.account.plan = self.plan
        self.user.account.save()

    def test_returns_status_ok(self):
        self.client.force_login(self.user)

        response = self.client.post(
            f'/api/images/{self.user_image.id}/generate_link/',
            data={
                'size': 0
            },
            content_type='application/json'
        )

        assert response.status_code == status.HTTP_200_OK

    def test_posting_data_with_size_0_creates_correct_image_link_instance(
            self
    ):
        self.client.force_login(self.user)

        self.client.post(
            f'/api/images/{self.user_image.id}/generate_link/',
            data={
                'size': 0
            },
            content_type = 'application/json'
        )

        image_link = ImageLink.objects.last()

        assert image_link.user_image == self.user_image
        assert image_link.thumbnail_size is None
        assert image_link.original_size is True

    def test_posting_data_with_size_1_creates_correct_image_link_instance(
            self
    ):
        thumbnail_size = ThumbnailSize.objects.create(
            name='Small',
            height=200,
            code=1
        )
        self.plan.thumbnail_sizes.add(thumbnail_size)
        self.client.force_login(self.user)

        self.client.post(
            f'/api/images/{self.user_image.id}/generate_link/',
            data={
                'size': 1
            },
            content_type='application/json'
        )

        image_link = ImageLink.objects.last()

        assert image_link.user_image == self.user_image
        assert image_link.thumbnail_size == thumbnail_size
        assert image_link.original_size is False