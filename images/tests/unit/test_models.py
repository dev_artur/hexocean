import tempfile
from datetime import timedelta
from uuid import uuid4

import pytest
from PIL import Image
from django.utils import timezone

from accounts.models import ThumbnailSize
from images.models import ImageLink
from images.models import UserImage
from images.tests.utils import get_test_image_file
from project.factories import UserFactory


@pytest.mark.django_db
class TestUserImage:

    def setup_method(self):
        self.user = UserFactory.create()

    def test_has_correct_fields(self):
        image = tempfile.NamedTemporaryFile(suffix='png').name
        user_image = UserImage.objects.create(
            user=self.user,
            image=image
        )

        assert user_image.user == self.user
        assert user_image.image == image


@pytest.mark.django_db
class TestImageLink:

    def setup_method(self):
        self.user = UserFactory.create()

    def test_has_correct_fields(self):
        image = tempfile.NamedTemporaryFile(suffix='png').name
        user_image = UserImage.objects.create(
            user=self.user,
            image=image
        )
        thumbnail_size = ThumbnailSize.objects.create(
            height=200,
            name='Small',
            code=1
        )
        link_uuid = str(uuid4())
        expires = timezone.now() + timedelta(seconds=300)

        image_link = ImageLink.objects.create(
            user_image=user_image,
            thumbnail_size=thumbnail_size,
            uuid=link_uuid,
            original_size=False,
            expires=expires
        )

        assert image_link.user_image == user_image
        assert image_link.thumbnail_size == thumbnail_size
        assert image_link.uuid == link_uuid
        assert image_link.original_size is False
        assert image_link.expires == expires

    def test_get_image_returns_original_image_when_original_size_is_true(self):
        test_image = get_test_image_file()
        user_image = UserImage.objects.create(
            user=self.user,
            image=test_image
        )
        link_uuid = str(uuid4())

        image_link = ImageLink.objects.create(
            user_image=user_image,
            thumbnail_size=None,
            uuid=link_uuid,
            original_size=True
        )

        image = image_link.get_image()

        assert image == Image.open(user_image.image.file)

    def test_get_image_returns_resized_image(self):
        test_image = get_test_image_file(size=(600,600))
        user_image = UserImage.objects.create(
            user=self.user,
            image=test_image
        )
        thumbnail_size = ThumbnailSize.objects.create(
            height=200,
            name='Small',
            code=1
        )
        link_uuid = str(uuid4())

        image_link = ImageLink.objects.create(
            user_image=user_image,
            thumbnail_size=thumbnail_size,
            uuid=link_uuid,
            original_size=False
        )

        image = image_link.get_image()

        assert image.size[1] == thumbnail_size.height