import pytest
from rest_framework.test import APIRequestFactory

from accounts.models import Plan
from accounts.models import ThumbnailSize
from images.models import ImageLink
from images.models import UserImage
from images.serializers import ImageLinkSerializer
from images.serializers import ImageSerializer
from images.serializers import ImageUploadSerializer
from images.tests.utils import get_test_image_file
from project.factories import UserFactory


@pytest.mark.django_db
class TestImageUploadSerializer:

    def setup_method(self):
        self.serializer_class = ImageUploadSerializer

    def test_serializer_invalid_without_image_data(self):
        serializer = self.serializer_class(data={})
        serializer.is_valid()

        assert serializer.errors.keys() == {'image'}

    def test_serializer_valid_with_correct_data(self):
        image_file = get_test_image_file()
        data = {
            'image': image_file
        }
        serializer = self.serializer_class(data=data)

        assert serializer.is_valid()

    def test_saving_serializer_creates_user_image_instance(self):
        user = UserFactory.create()
        image_file = get_test_image_file()
        data = {
            'image': image_file
        }
        serializer = self.serializer_class(data=data, context={'user': user})
        serializer.is_valid()
        serializer.save()

        user_image = UserImage.objects.last()

        assert user_image.user == user
        assert user_image.image.file.size == image_file.size


@pytest.mark.django_db
class TestImageLinkSerializer:

    @classmethod
    @pytest.fixture(autouse=True)
    def setup_class(cls, db):
        cls.serializer_class = ImageLinkSerializer
        cls.user = UserFactory.create()
        cls.thumbnail_size_1 = ThumbnailSize.objects.create(
            name='Test',
            height=200,
            code=1
        )
        cls.thumbnail_size_2 = ThumbnailSize.objects.create(
            name='Test',
            height=200,
            code=2
        )
        cls.plan = Plan.objects.create(name='Basic')
        cls.plan.thumbnail_sizes.add(cls.thumbnail_size_1)
        cls.user.account.plan = cls.plan
        cls.user.account.save()
        cls.image_file = get_test_image_file()
        cls.user_image = UserImage.objects.create(
            user=cls.user,
            image=cls.image_file
        )

    def test_is_invalid_when_size_does_not_exist(self):
        serializer = self.serializer_class(
            data={'size': 3}, context={'user': self.user}
        )

        assert not serializer.is_valid()
        assert 'size' in serializer.errors

    def test_is_invalid_when_size_not_in_user_account(self):
        serializer = self.serializer_class(
            data={'size': 2}, context={'user': self.user}
        )

        assert not serializer.is_valid()
        assert 'size' in serializer.errors

    def test_is_invalid_when_size_0_but_not_in_user_account(self):
        self.plan.original_images = False
        self.plan.save()
        serializer = self.serializer_class(
            data={'size': 0}, context={'user': self.user}
        )

        assert not serializer.is_valid()
        assert 'size' in serializer.errors

    def test_is_valid_when_size_0_and_in_user_account(self):
        self.plan.original_images = True
        self.plan.save()
        serializer = self.serializer_class(
            data={'size': 0}, context={'user': self.user}
        )

        assert serializer.is_valid()

    def test_is_valid_when_size_in_user_account(self):
        serializer = self.serializer_class(
            data={'size': 1}, context={'user': self.user}
        )

        assert serializer.is_valid()

    def test_is_invalid_when_expires_after_too_low(self):
        serializer = self.serializer_class(
            data={'size': 1, 'expires_after': 299}, context={'user': self.user}
        )

        assert not serializer.is_valid()
        assert 'expires_after' in serializer.errors

    def test_is_invalid_when_expires_after_too_high(self):
        serializer = self.serializer_class(
            data={'size': 1, 'expires_after': 30001}, context={'user': self.user}
        )

        assert not serializer.is_valid()
        assert 'expires_after' in serializer.errors

    def test_is_invalid_when_expires_after_not_in_user_plan(self):
        self.user.account.plan.expiring_links = False
        self.user.account.plan.save()
        serializer = self.serializer_class(
            data={'size': 1, 'expires_after': 300}, context={'user': self.user}
        )

        assert not serializer.is_valid()
        assert 'expires_after' in serializer.errors

    def test_creates_new_image_link_with_thumbnail_size(self):
        serializer = self.serializer_class(
            data={'size': 1},
            context={
                'user': self.user,
                'user_image': self.user_image
            }
        )
        serializer.is_valid()

        new_link = serializer.save()
        link = ImageLink.objects.last()

        assert new_link == link
        assert new_link.user_image == self.user_image
        assert new_link.thumbnail_size == self.thumbnail_size_1
        assert new_link.original_size is False

    def test_creates_new_image_link_with_original_size(self):
        self.plan.original_images = True
        self.plan.save()
        serializer = self.serializer_class(
            data={'size': 0},
            context={
                'user': self.user,
                'user_image': self.user_image
            }
        )
        serializer.is_valid()

        new_link = serializer.save()
        link = ImageLink.objects.last()

        assert new_link == link
        assert new_link.user_image == self.user_image
        assert new_link.thumbnail_size is None
        assert new_link.original_size is True

    def test_save_returns_instance_with_thumbnail_size_if_already_exists(self):
        old_link = ImageLink.objects.create(
            user_image=self.user_image, thumbnail_size=self.thumbnail_size_1
        )
        serializer = self.serializer_class(
            data={'size': 1},
            context={
                'user': self.user,
                'user_image': self.user_image
            }
        )
        serializer.is_valid()

        new_link = serializer.save()

        assert new_link == old_link

    def test_save_returns_instance_with_original_size_if_already_exists(self):
        self.plan.original_images = True
        self.plan.save()
        old_link = ImageLink.objects.create(
            user_image=self.user_image, original_size=True
        )
        serializer = self.serializer_class(
            data={'size': 0},
            context={
                'user': self.user,
                'user_image': self.user_image
            }
        )
        serializer.is_valid()

        new_link = serializer.save()

        assert new_link == old_link

    def test_creates_instance_with_expiry_date_if_passed_in_data(self):
        self.user.account.plan.expiring_links = True
        self.user.account.plan.save()
        serializer = self.serializer_class(
            data={
                'size': 1,
                'expires_after': 300,
            },
            context={
                'user': self.user,
                'user_image': self.user_image
            }
        )
        serializer.is_valid()
        link = serializer.save()

        assert link.expires is not None


@pytest.mark.django_db
class TestImageSerializer:

    def setup_method(self):
        self.user = UserFactory.create()
        self.image_file = get_test_image_file()
        self.user_image = UserImage.objects.create(
            user=self.user,
            image=self.image_file
        )
        self.serializer_class = ImageSerializer

    def test_serializes_correctly(self):
        factory = APIRequestFactory()
        request = factory.get('')
        serializer = self.serializer_class(
            self.user_image, context={'request': request}
        )
        expected_data = {
            'id': self.user_image.id,
            'generate_link_url': f'http://testserver/api/images'
                                  f'/{self.user_image.id}/generate_link/',
            'name': self.user_image.image.name
        }

        assert serializer.data == expected_data

