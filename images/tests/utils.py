from io import BytesIO

from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile


def get_test_image_file(extension='JPEG', name='image', size=(100, 100)):
    image = BytesIO()
    Image.new('RGB', size).save(image, extension)
    image.seek(0)
    return SimpleUploadedFile(f'{name}.{extension}', image.getvalue())