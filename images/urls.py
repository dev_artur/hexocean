from django.urls import path

from images import views

api_urlpatterns = [
    path('', views.ImagesListView.as_view()),
    path('upload_image/', views.ImagesView.as_view()),
    path('<int:pk>/generate_link/', views.GenerateLinkView.as_view()),
]

media_urlpatterns = [
    path('<uuid:uuid>/', views.ImageLinkView.as_view())
]