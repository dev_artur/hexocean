from uuid import uuid4

from PIL import Image
from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator
from django.db import models

from accounts.models import ThumbnailSize
from images.processing import get_resized_image


class UserImage(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    image = models.ImageField(
        validators=[
            FileExtensionValidator(
                allowed_extensions=settings.ALLOWED_IMAGE_EXTENSIONS
            )
        ]
    )


class ImageLink(models.Model):
    user_image = models.ForeignKey(
        UserImage,
        on_delete=models.CASCADE
    )
    thumbnail_size = models.ForeignKey(
        ThumbnailSize,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )
    uuid = models.UUIDField(default=uuid4)
    original_size = models.BooleanField(default=False)
    expires = models.DateTimeField(null=True)

    class Meta:
        indexes = [
            models.Index(fields=['uuid'])
        ]

    def get_image(self):
        if self.original_size:
            return Image.open(self.user_image.image.file)
        else:
            return get_resized_image(
                self.user_image.image.file, self.thumbnail_size.height
            )