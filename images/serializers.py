from datetime import timedelta
from urllib.parse import urlparse

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from accounts.models import Account
from accounts.models import ThumbnailSize
from images.models import ImageLink
from images.models import UserImage


class ImageUploadSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserImage
        fields = ('image', )

    def create(self, validated_data):
        validated_data['user'] = self.context['user']
        return super().create(validated_data)


class ImageLinkSerializer(serializers.Serializer):
    size = serializers.IntegerField()
    expires_after = serializers.IntegerField(
        required=False,
        min_value=settings.EXPIRES_AFTER_MIN,
        max_value=settings.EXPIRES_AFTER_MAX
    )

    def validate_size(self, value):
        user = self.context['user']
        user_account = Account.objects.prefetch_related(
            'plan__thumbnail_sizes'
        ).get(user=user)

        if value == 0:
            if user_account.plan.original_images:
                return value
            else:
                raise ValidationError(
                    'User plan does not allow original sizes'
                )

        try:
            thumbnail_size = ThumbnailSize.objects.get(
                code=value
            )
            if not thumbnail_size in user_account.plan.thumbnail_sizes.all():
                raise ValidationError('User plan does not allow this size')
        except ObjectDoesNotExist:
            raise ValidationError('Invalid size')

        return value

    def validate_expires_after(self, value):
        user = self.context['user']
        if not user.account.plan.expiring_links:
            raise ValidationError('User plan does not allow expiring links')

        return value

    def create(self, validated_data):
        create_kwargs = {
            'user_image': self.context['user_image']
        }

        expires_after = validated_data.get('expires_after')
        if expires_after:
            expires = timezone.now() + timedelta(seconds=expires_after)
            create_kwargs['expires'] = expires

        if validated_data['size'] == 0:
            create_kwargs['original_size'] = True
        else:
            thumbnail_size = ThumbnailSize.objects.get(
                code=validated_data['size']
            )
            create_kwargs['thumbnail_size'] = thumbnail_size
        image_link, created = ImageLink.objects.get_or_create(**create_kwargs)
        return image_link


class ImageSerializer(serializers.ModelSerializer):
    generate_link_url = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    class Meta:
        model = UserImage
        fields = ('id', 'generate_link_url', 'name')

    def get_generate_link_url(self, obj):
        request = self.context['request']
        parsed_uri = urlparse(request.build_absolute_uri())
        url = f'{parsed_uri.scheme}://{parsed_uri.netloc}/api/images/' \
              f'{obj.id}/generate_link/'
        return url

    def get_name(self, obj):
        return obj.image.name
