from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.utils import timezone
from django.views.generic import DetailView
from rest_framework import permissions
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from images.models import ImageLink
from images.models import UserImage
from images.serializers import ImageLinkSerializer
from images.serializers import ImageSerializer
from images.serializers import ImageUploadSerializer


class ImagesView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ImageUploadSerializer

    def post(self, request, *args, **kwargs):
        upload_serializer = self.serializer_class(
            data=request.data, context={'user': request.user}
        )
        upload_serializer.is_valid(raise_exception=True)
        user_image = upload_serializer.save()

        serializer = ImageSerializer(user_image, context={'request': request})
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ImagesListView(ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ImageSerializer

    def get_queryset(self):
        queryset = UserImage.objects.filter(user=self.request.user)
        return queryset


class ImageLinkView(DetailView):
    model = ImageLink

    def get_object(self, queryset=None):
        try:
            model = ImageLink.objects.select_related(
                'user_image'
            ).get(
                Q(expires__gte=timezone.now()) | Q(expires__isnull=True),
                uuid=self.kwargs.get('uuid')
            )
            return model
        except ObjectDoesNotExist:
            raise Http404('Link with this uuid does not exist')

    def get(self, request, *args, **kwargs):
        image = self.get_image_object()
        response = HttpResponse(content_type="image/jpeg")
        image.save(response, "JPEG")
        return response

    def get_image_object(self):
        image_link = self.get_object()
        return image_link.get_image()


class GenerateLinkView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ImageLinkSerializer

    def post(self, request, pk):
        user_image = get_object_or_404(UserImage, pk=pk)
        serializer = self.serializer_class(
            data=request.data,
            context={
                'user': request.user,
                'user_image': user_image
            }
        )
        serializer.is_valid(raise_exception=True)
        image_link = serializer.save()
        return Response(
            {
                'link': request.build_absolute_uri(
                    f'/media/{image_link.uuid}/'
                )
            }
        )