from PIL import Image


def get_resized_image(image, height):
    image_file = Image.open(image)

    if height >= float(image_file.size[1]):
        return image_file
    else:
        ratio = height / float(image_file.size[1])
        size = (
            int(float(image_file.size[0]) * float(ratio)),
            int(float(image_file.size[1]) * float(ratio))
        )
        resized_image = image_file.resize(size, Image.ANTIALIAS)
        return resized_image
