from django.contrib import admin
from django.urls import include
from django.urls import path

from images.urls import api_urlpatterns as images_api_urls
from images.urls import media_urlpatterns as media_urls

admin.autodiscover()
admin.site.enable_nav_sidebar = False

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/images/', include(images_api_urls)),
    path('media/', include(media_urls)),
    path('api-auth/', include('rest_framework.urls')),
]
