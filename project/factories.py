import factory
from django.contrib.auth.models import User
from faker import Factory

faker = Factory.create()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = factory.LazyFunction(faker.first_name)
    email = factory.LazyFunction(faker.email)
    username = factory.LazyAttribute(lambda u: u.email)
    password = factory.PostGenerationMethodCall('set_password', 'password')
