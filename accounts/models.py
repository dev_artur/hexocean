from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models


class ThumbnailSize(models.Model):
    name = models.CharField(max_length=15)
    height = models.IntegerField()
    code = models.IntegerField(validators=[MinValueValidator(1)], unique=True)

    class Meta:
        indexes = [
            models.Index(fields=['code'])
        ]

    def __str__(self):
        return f'{self.name}, height: {self.height}px'


class Plan(models.Model):
    name = models.CharField(max_length=30)
    original_images = models.BooleanField(default=False)
    expiring_links = models.BooleanField(default=False)
    thumbnail_sizes = models.ManyToManyField(ThumbnailSize)

    def __str__(self):
        return f'{self.name} plan'


class Account(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='account'
    )
    plan = models.ForeignKey(
        Plan,
        blank=False,
        null=True,
        on_delete=models.SET_NULL
    )

    def __str__(self):
        return f'{self.user.username} account'