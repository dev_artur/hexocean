import pytest

from accounts.models import Account
from accounts.models import Plan
from accounts.models import ThumbnailSize
from project.factories import UserFactory


@pytest.mark.django_db
class TestPlan:

    def test_has_correct_fields(self):
        plan = Plan.objects.create(
            name='Basic',
            original_images=True,
            expiring_links=True
        )

        assert plan.name == 'Basic'
        assert plan.original_images is True
        assert plan.expiring_links is True

    def test_has_many_to_many_relation_to_thumbnail_size(self):
        plan = Plan.objects.create(
            name='Basic',
            original_images=True,
            expiring_links=True
        )
        thumbnail_size_1 = ThumbnailSize.objects.create(
            name='Small',
            height=200,
            code=1
        )
        thumbnail_size_2 = ThumbnailSize.objects.create(
            name='Medium',
            height=400,
            code=2
        )
        plan.thumbnail_sizes.add(*[thumbnail_size_1, thumbnail_size_2])

        assert plan.thumbnail_sizes.count() == 2
        assert set(plan.thumbnail_sizes.all()) == {
            thumbnail_size_1, thumbnail_size_2
        }


@pytest.mark.django_db
class TestThumbnailSize:

    def test_has_correct_fields(self):
        thumbnail_size = ThumbnailSize.objects.create(
            name='Small',
            height=200,
            code=1
        )

        assert thumbnail_size.name == 'Small'
        assert thumbnail_size.height == 200
        assert thumbnail_size.code == 1


@pytest.mark.django_db
class TestAccount:

    def test_is_created_after_user_is_created(self):
        user = UserFactory.create()

        assert Account.objects.filter(user=user).exists()

    def test_has_correct_fields(self):
        user = UserFactory.create()
        plan = Plan.objects.create(name='Test')

        account = Account.objects.get(user=user)
        account.plan = plan
        account.save()

        assert account.user == user
        assert account.plan == plan
