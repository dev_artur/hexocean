# Generated by Django 3.1.4 on 2020-12-17 05:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ThumbnailSize',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=15)),
                ('height', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='plan',
            name='thumbnail_sizes',
            field=models.ManyToManyField(to='accounts.ThumbnailSize'),
        ),
    ]
