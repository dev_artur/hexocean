from django.contrib import admin

from accounts.models import Account
from accounts.models import Plan
from accounts.models import ThumbnailSize


class ThumbnailSizeInline(admin.TabularInline):
    model = Plan.thumbnail_sizes.through
    extra = 1


class PlanAdmin(admin.ModelAdmin):
    list_display = ('name',)
    fields = ('name', 'original_images', 'expiring_links')
    inlines = [ThumbnailSizeInline]


class ThumbnailSizeAdmin(admin.ModelAdmin):
    list_display = ('name', 'height', 'code')
    fields = ('name', 'height', 'code')


class AccountAdmin(admin.ModelAdmin):
    list_display = ('user', 'plan')
    fields = ('user', 'plan')


admin.site.register(Account, AccountAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(ThumbnailSize, ThumbnailSizeAdmin)